# ctrace

![](https://img.shields.io/badge/written%20in-C-blue)

A simple multithreaded raytracing engine.

Tags: graphics


## Download

- [⬇️ ctrace-report-redacted.pdf](dist-archive/ctrace-report-redacted.pdf) *(184.67 KiB)*
- [⬇️ ctrace-final-gcode.7z](dist-archive/ctrace-final-gcode.7z) *(311.84 KiB)*
